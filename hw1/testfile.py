import numpy as np
import matplotlib.pyplot as plt

from util import *
from hw1 import *

image = load_image('data/69015.jpg')
box = np.array([[1, 1, 1, 1, 1],[1, 1, 1, 1, 1],[1, 1, 1, 1, 1],[1, 1, 1, 1, 1],[1, 1, 1, 1, 1]])
img = conv_2d(image,box)

plt.figure(); plt.imshow(image, cmap='gray')
plt.figure(); plt.imshow(img, cmap='gray')
plt.show()

#print(gaussian_1d())
#print(gaussian_1d(2.0))

#image = load_image('data/148089_noisy.png')
image = load_image('data/87046.jpg')
imgA  = denoise_gaussian(image, 0.5)
imgB  = denoise_gaussian(image, 2.5)

plt.figure(); plt.imshow(image, cmap='gray')
plt.figure(); plt.imshow(imgA, cmap='gray')
plt.figure(); plt.imshow(imgB, cmap='gray')
plt.show()

image = load_image('data/143090_noisy.png')
imgA  = denoise_median(image, 1)
imgB  = denoise_median(image, 2)

plt.figure(); plt.imshow(image, cmap='gray')
plt.figure(); plt.imshow(imgA, cmap='gray')
plt.figure(); plt.imshow(imgB, cmap='gray')
plt.show()

image  = load_image('data/69015.jpg')
dx, dy = sobel_gradients(image)

plt.figure(); plt.imshow(image, cmap='gray')
plt.figure(); plt.imshow(dx, cmap='gray')
plt.figure(); plt.imshow(dy, cmap='gray')
plt.show()


image  = load_image('data/78004.jpg')
mag, nonmax = canny(image)
plt.figure(); plt.imshow(mag, cmap='gray')
plt.figure(); plt.imshow(nonmax, cmap='gray')
plt.show()

image  = load_image('data/87046.jpg')
mag, nonmax = canny(image)
plt.figure(); plt.imshow(mag, cmap='gray')
plt.figure(); plt.imshow(nonmax, cmap='gray')
plt.show()

image  = load_image('data/295087.jpg')
mag, nonmax = canny(image)
plt.figure(); plt.imshow(mag, cmap='gray')
plt.figure(); plt.imshow(nonmax, cmap='gray')
plt.show()
