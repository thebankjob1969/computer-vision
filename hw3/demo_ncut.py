"""
   Demo: Spectral clustering based on Normalized Cut.
"""

import numpy as np
import matplotlib.pyplot as plt

from superpixels import *
from ncut import *
from util import *

# load example image
img = load_rgb_image('data/demo/69015.jpg')

# compute superpixels
seg = superpixels(img)

# build region graph
adjacency_mx, region_pixels = seg2graph(seg)

# load groundtruth segmentation
seg_gt = load_seg_gt('data/demo/69015_gt.png')

# project groundtruth onto superpixels
region_gt = proj_onto_regions(region_pixels, seg_gt, 'mode')

"""
   Construct a pairwise affinity matrix for neighboring regions using the
   ground-truth segmentation, with some random noise added.

   This is perfect affinity matrix (which we want to predict) plus some noise.

   Setting noise_level = 0 corresponds to having perfect affinity estimates.
   Setting noise_level = 1 corresponds to having random affinity estimates.
"""
noise_level = 0.5
n_reg = region_gt.size
W = np.zeros((n_reg, n_reg))
for r0 in range(n_reg):
   for r1 in range(n_reg):
      noise = noise_level * np.random.rand(1);
      W[r0,r1] = (region_gt[r0] == region_gt[r1]) * (1.0 - noise_level) + noise

"""
   Recover a region embedding from the noisy affinity estimates in W.

   For N regions, W is an N x N matrix.
   We recover a N x k matrix storing a k-dimensional feature for each region.
   Regions with similar feature embeddings should be grouped together.
"""
k = 8
evec = ncut(W, k)

evec012 = render_onto_seg(seg, region_pixels, evec[:,0:3])
evec345 = render_onto_seg(seg, region_pixels, evec[:,3:6])

plt.figure(); plt.imshow(evec012);
plt.title('First 3 dimensions of region feature embedding');
plt.figure(); plt.imshow(evec345)
plt.title('Dimensions 4-6 of region feature embedding');
plt.show()

