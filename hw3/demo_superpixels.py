"""
   Demo: superpixel oversegmentation
"""
import numpy as np
import matplotlib.pyplot as plt

from superpixels import *
from util import *

# load example image
img = load_rgb_image('data/demo/69015.jpg')

# compute superpixels
seg = superpixels(img)

# build region graph
adjacency_mx, region_pixels = seg2graph(seg)

# project image into regions (average color over regions)
region_colors = proj_onto_regions(region_pixels, img, 'mean')

# render average region color as an image
seg_colors = render_onto_seg(seg, region_pixels, region_colors)

# load groundtruth segmentation
seg_gt = load_seg_gt('data/demo/69015_gt.png')

# project groundtruth onto superpixels
region_gt = proj_onto_regions(region_pixels, seg_gt, 'mode')
seg_gt_proj = render_onto_seg(seg, region_pixels, region_gt)

# display image and superpixel segmenttation
plt.figure(); plt.imshow(img);         plt.title('image')
plt.figure(); plt.imshow(seg);         plt.title('superpixels')
plt.figure(); plt.imshow(seg_colors);  plt.title('mean color on superpixels')
plt.figure(); plt.imshow(seg_gt);      plt.title('groundtruth segmentation')
plt.figure(); plt.imshow(seg_gt_proj); plt.title('groundtruth on superpixels')
plt.show()
