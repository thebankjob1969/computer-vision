"""
   Superpixel oversegmentation.
"""

import numpy as np
import scipy.ndimage
import scipy.stats
import skimage.feature
import skimage.morphology

from color import *
from filters import *

"""
   Superpixel oversegmentation.

   Arguments:
      img     - RGB or grayscale image as a numpy array
      th_dist - distance (in pixels) threshold on superpixel centers

      Increasing th_dist pushes superpixel centers farther apart, making
      individual superpixels larger, and reducing the total number of
      superpixels.

   Returns:
      seg - a 2D numpy array containing a superpixel oversegmentation of the
            image; each pixel is assigned the integer id of the region to
            which it belongs
"""
def superpixels(img, th_dist = 7):
   # convert image to grayscale
   if (img.ndim == 3):
      img = rgb2gray(img)
   assert img.ndim == 2, 'image should be grayscale'
   # compute edge strength from multiscale local oriented gradients
   edge = np.zeros(img.shape)
   for scale in [2.0, 4.0]:
      for ori in range(8):
         filt = gaussian_deriv_2d(scale, 1, 0, (ori/8)*np.pi)
         grad = scipy.signal.convolve2d( \
                  img, filt, mode = 'same', boundary = 'symm')
         edge = edge + np.abs(grad)
   edge = edge / np.amax(edge)
   # find local minima in edge strength
   e_level = np.round(edge * 1000)
   lm = skimage.feature.peak_local_max( \
      -e_level, min_distance = th_dist, indices=False)
   markers = scipy.ndimage.label(lm)[0]
   # perform watershed oversegmentation
   seg = skimage.morphology.watershed(e_level, markers, compactness=0.5)
   seg = seg - 1  # switch to zero-based indexing of regions
   # randomly permute region labels for easier visualization
   rperm = np.random.permutation(np.amax(seg)+1)
   seg = rperm[seg]
   return seg

"""
   Build a graph from a superpixel (or other) segmentation.

   Regions (e.g. superpixels) correspond to vertices in the graph.
   Boundaries between regions correspond to edges in the graph.

   Given an input segmentation, in the form of a region id assignment for each
   pixel, this method builds a region adjacency matrix and a list of pixels
   contained in each region.

   Region pixels are listed according to their linear index.  To convert back
   to 2D coordinates, use the numpy.unravel_index( ... ) function.

   Arguments:
      seg - a 2D numpy array containing a segmentation of the image; each pixel
            is assigned the integer id of the region to which it belongs

   Returns:
      adjacency_mx   - region adjacency indicator matrix
      region_pixels  - list of pixels contained in each region
"""
def seg2graph(seg):
   # get image size
   assert seg.ndim == 2, 'segmentation should be 2D'
   sx, sy = seg.shape
   # get region count
   n_reg = np.amax(seg) + 1
   # initialize adjacency indicator matrix
   adjacency_mx = np.zeros((n_reg, n_reg))
   # initialize region pixel lists
   region_pixels = []
   for n in range(n_reg):
      region_pixels.append([])
   # detect neighboring regions
   for x in range(sx):
      xa = max(0, x - 1)
      xb = min(x + 2, sx)
      for y in range(sy):
         ya = max(0, y - 1)
         yb = min(y + 2, sy)
         r = seg[x,y]                  # region id at current location
         r_ids = seg[xa:xb, ya:yb]     # ids of neighboring regions
         r_ids = r_ids.flatten()
         # update adjacency matrix
         adjacency_mx[r,r_ids] = 1
         adjacency_mx[r_ids,r] = 1
         # update pixel list
         px_id = x * sy + y
         region_pixels[r].append(px_id)
   return adjacency_mx, region_pixels

"""
   Project a signal onto regions in a segmentation.

   Given a signal with same spatial extent as the original image, project that
   signal onto regions by applying the given summariztion operation.  If signal
   is a 3D numpy array, apply the operation to each channel.

   Arguments:
      region_pixels - list of pixels contained in each region (from seg2graph)
      signal        - 2D or 3D numpy array to project onto regions
      op            - operation over regions: 'mean' or 'median' or 'mode'

   Returns:
      region_vals   - a numpy array containing values for each region id
"""
def proj_onto_regions(region_pixels, signal, op):
   # get signal shape
   signal = np.atleast_3d(signal)
   sx, sy, nc = signal.shape
   signal = np.reshape(signal, (sx*sy, nc))
   # get operator
   if op == 'mean':
      f = np.mean
   elif op == 'median':
      f = np.median
   elif op == 'mode':
      def f_mode(x):
         return scipy.stats.mode(x)[0]
      f = f_mode
   else:
      raise ValueError('invalid op specified')
   # initialize output
   n_reg = len(region_pixels)
   region_vals = np.zeros((n_reg, nc))
   for r in range(n_reg):
      sig = signal[region_pixels[r],:]
      for c in range(nc):
         region_vals[r,c] = f(sig[:,c])
   return region_vals

"""
   Render a function defined on regions as a signal over the spatial extent of
   the original image.

   Arguments:
      seg           - segmentation of the image
      region_pixels - list of pixels contained in each region (from seg2graph)
      region_vals   - values (1 or more channels) for each region

   Returns:
      signal        - a 2D or 3D numpy array of the spatial extent as the
                      original image
"""
def render_onto_seg(seg, region_pixels, region_vals):
   # initialize output
   sx, sy = seg.shape
   n_reg, nc = region_vals.shape
   signal = np.zeros((sx*sy,nc))
   # render
   for r in range(n_reg):
      px_ids = region_pixels[r]
      signal[px_ids,:] = region_vals[r,:]
   # reshape
   if (nc == 1):
      signal = np.reshape(signal,(sx,sy))
   else:
      signal = np.reshape(signal,(sx,sy,nc))
   return signal
